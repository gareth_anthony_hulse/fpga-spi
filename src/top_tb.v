`timescale 10ns/100ps

`include "spi.v"

module top_tb;
   output wire	  miso_sig,
		  cs_sig,
		  sclk_sig,
		  mosi_sig;
   output wire [3:0] count_sig;
   logic [3:0]	counter;
   wire	clock;
   

   initial
     begin
	$dumpfile(`OUTPUT);
	$dumpvars(0,top_tb);
	// Simulation time limit:
	#`TIME_LIMIT $finish;
     end

      spi spi_inst (.clk (clock),
		 .sclk (clock),
		 .mosi (mosi_sig),
		 .miso (miso_sig),
		 .cs (cs_sig),
		 .count (count_sig));
   
   always
     begin
	#50 clock = !clock;	
     end
endmodule  
   
