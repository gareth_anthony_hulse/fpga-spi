module spi (
	    input wire	       hwclk, clk, miso,
	    input logic [0:23]  data,
	    output logic       cs, mosi,
	    output wire	       sclk
	    );
   logic [7:0]		       count;
   
   initial begin
	count <= 8'd0;
	cs <= 1;
     end

   always @(posedge clk) begin
	if (count == 0) begin
	     cs <= 0;
	  end
	else if (count == 24) begin
	   cs <= 1;
	end
     end

   always @(negedge clk) begin
      count <= count + 8'd1;
      if (count >= 24) begin
	 count <= 0;	 
      end
   end
   
   assign sclk = cs ? 0 :
		 clk;
   assign mosi = data[count];
endmodule
