`include "spi.v"

module top (
	    input  HWCLK, MISO,
	    output MOSI, CS, SCLK
	    );
   logic [20:0]	   divider = 0;
   logic	   sclk_sig = 0;
   
   spi my_spi(.hwclk (HWCLK),
	      .clk (sclk_sig),
	      .sclk (SCLK),
	      .mosi (MOSI),
	      .miso (MISO),
	      .cs (CS),
	      .data (24'b000000011001000000000000));
   
   always @(posedge HWCLK) begin
      if (divider != 7) begin
	 divider <= divider + 1;
      end
      else begin
	 divider <= 0;
	 sclk_sig <= sclk_sig + 1;
      end
   end
endmodule
