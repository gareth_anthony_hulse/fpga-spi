project_name = fpga-spi
SRC_DIR = $(realpath  .)/src
BUILD_DIR = $(realpath .)/build
output = ${BUILD_DIR}/${project_name}
DEFINITIONS = -DOUTPUT=\"${output}\" -DTIME_LIMIT=`expr 10^5 | bc`
# 10^5 = 1ms.

.PHONY: init install clean

${output}: ${output}.asc
	icepack $< $@

${output}.asc: ${output}.json
	nextpnr-ice40 --hx8k --package ct256 --json $< --pcf src/pinmap.pcf --asc $@

${output}.json: init
	yosys -q -p "verilog_defines ${DEFINITIONS}; read_verilog -sv src/top.v; read_verilog -lib /usr/share/yosys/ice40/cells_sim.v; hierarchy -check -auto-top; proc; opt; fsm; opt; memory; opt; aigmap; synth_ice40 -json $@"

init:
	mkdir -p ${BUILD_DIR}

install:
	# If `${output}` cannot be found, run `make`. Else, just install.
	! [ -f ${output} ] && make && iceprog ${output} || iceprog ${output}

clean:
	# If `build` exists, remove it. Otherwise, just exit with `true` to avoid an error.
	[ -f build ] || [ -d build ] && rm -rf build || true

show: ${output}.json
	 nextpnr-ice40 --hx8k --package ct256 --json $< --pcf src/pinmap.pcf --gui

svg: ${output}.json
	netlistsvg "${output}.json" -o "${output}.svg"

simulate: init
	iverilog ${DEFINITIONS} "${SRC_DIR}/top_tb.v"  -o "${output}.vvp" -I "${SRC_DIR}"
	vvp "${output}.vvp"
